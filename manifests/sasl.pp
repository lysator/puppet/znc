# @summary Configures central authentication
# 
# Configures saslauthd to allow authentication through pam,
#
# Also loads the ZNC cyrusauth module and configures it to use
# saslauth.
#
# @param package
#   Name of the package providing saslauthd
class znc::sasl (
  String $package = 'sasl2-bin',
) {
  ensure_packages([$package], { ensure => installed, })

  file_line { 'saslauthd START=yes':
    ensure  => present,
    path    => '/etc/default/saslauthd',
    line    => 'START=yes',
    match   => '^START=',
    require => Package[$package],
    notify  => Service['saslauthd'],
  }

  file_line { 'saslauthd pam':
    ensure  => present,
    path    => '/etc/default/saslauthd',
    line    => 'MECHANISMS="pam"',
    match   => '^MECHANISMS=',
    require => Package[$package],
    notify  => Service['saslauthd'],
  }

  service { 'saslauthd':
    ensure => running,
    enable => true,
  }

  file { "${znc::setup::datadir}/moddata/cyrusauth":
    ensure => directory,
    owner  => $znc::setup::user,
  }

  znc::module { 'cyrusauth':
    args => ['saslauthd'],
  }
}
