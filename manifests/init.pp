# @summary
class znc {
  require znc::setup

  service { 'znc':
    ensure => running,
    enable => true,
    # subscribe => [ Class['::znc::setup'], ],
  }
}
