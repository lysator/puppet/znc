# @summary Configures ident
#
# https://en.wikipedia.org/wiki/Ident_protocol
class znc::ident {
  ensure_packages(['oidentd',], { ensure => installed, })

  $oident_conf = @("EOF")
    user "${znc::setup::user}" {
      default {
        allow spoof
        allow spoof_all
      }
    }
    |- EOF

  file { '/etc/oidentd.conf':
    ensure  => file,
    group   => $znc::setup::user,
    mode    => '0664',
    content => $oident_conf,
  }

  service { 'oidentd':
    ensure => running,
    enable => true,
  }

  znc::module { 'identfile': }

  file { "${znc::setup::datadir}/moddata/identfile":
    ensure => directory,
    owner  => $znc::setup::user,
  }

  file { "${znc::setup::datadir}/moddata/identfile/.registry":
    ensure  => file,
    owner   => $znc::setup::user,
    content => [
      'File /etc/oidentd.conf',
      'Format global { reply "%user%" }',
      '', # Trailing newline
    ].map |$s| { percent_escape($s) }.join("\n"),
  }
}
