# @summary Load a ZNC module
#
# Adds a module to the list of loaded modules.
#
# Note that this module doesn't provide the modules.
#
# @param module
#   Name of the module to load
# @param args
#   Extra arguments to pass to module.
define znc::module (
  String $module = $name,
  Array[String] $args = [],
) {
  $arg_str = join($args, ' ')

  file_line { "ZNC module ${module}":
    ensure => present,
    path   => '/var/lib/znc/configs/znc.conf',
    match  => "^LoadModule = ${module}",
    line   => "LoadModule = ${module} ${arg_str}",
  }
}
