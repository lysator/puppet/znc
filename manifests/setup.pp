# @summary Initial configuration of ZNC
# @api private
class znc::setup (
  Stdlib::Absolutepath $datadir = '/var/lib/znc',
  String $user                  = 'znc',
  String $package               = 'znc',
) {
  file { [
      $datadir,
      "${datadir}/configs",
      "${datadir}/mobdata",
    ]:
      ensure => directory,
      owner  => $user,
  }

  ensure_packages([$package], { ensure => latest, })

  # restart saslauthd here?

  user { $user:
    comment => 'ZNC Daemon runner',
    home    => $datadir,
    system  => true,
    shell   => '/usr/sbin/nologin',
    groups  => ['sasl',],
  }

  file { "${datadir}/configs/znc.conf":
    ensure  => file,
    replace => no,
    source  => "puppet:///modules/${module_name}/znc.conf",
    owner   => $user,
  }

  $certname = $facts['networking']['fqdn']

  file_line { 'Set ZNC SSL Cert File':
    path  => '/var/lib/znc/configs/znc.conf',
    match => '^SSLCertFile',
    line  => "SSLCertFile = /etc/letsencrypt/live/${certname}/fullchain.pem",
  }
  file_line { 'Set ZNC SSL DH Param File':
    path  => '/var/lib/znc/configs/znc.conf',
    match => '^SSLDHParamFile',
    line  => "SSLDHParamFile = /etc/letsencrypt/live/${certname}/fullchain.pem",
  }
  file_line { 'Set ZNC SSL Key File':
    path  => '/var/lib/znc/configs/znc.conf',
    match => '^SSLKeyFile',
    line  => "SSLKeyFile = /etc/letsencrypt/live/${certname}/privkey.pem",
  }

  znc::module { [
      'webadmin',
      'fail2ban',
      'chansaver',
    ]:
  }

  # Möjliga standarder för nya användare?
  # Gör så play-back ligger kvar även efter man sätt dem.
  # <user>
  #   AutoClearChanBuffer = false
  #   AutoClearQueryBuffer = false
  # </user>
  #
  # Se möjligen även över loggar

  systemd::unit_file { 'znc.service':
    source => "puppet:///modules/${module_name}/znc.service",
    before => Service['znc'],
  }
}
