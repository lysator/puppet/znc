ZNC IRC Bouncer
===============

https://wiki.znc.in/ZNC

```puppet
# Adds a basic ZNC configuration
include ::znc

# Adding this configures cyrusauth and SASL authentication, through PAM. 
# Note that PAM needs to be configured separately.
include ::znc::sasl

# Adds a basic identd instance
include ::znc::ident

# Additional ZNC modules can be loaded through
class znc::module { 'extra-module':
}
```
