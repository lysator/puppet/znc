# frozen_string_literal: true

# @summary Percent encode the given string
#
# Replaces all but the most absic ASCII characters with percent
# encoded bytes. For example, "/Hello" becomes "%2FHello". Note that
# characters encoded with multiple bytes in UTF-8 will become multiple
# bytes.
#
# @see https://en.wikipedia.org/wiki/Percent-encoding
Puppet::Functions.create_function(:percent_escape) do
  # @param string
  #   The string to encode
  # @return
  #   The string percent encoded.
  dispatch :percent_escape do
    param 'String', :string
  end

  def percent_escape(string)
    safe = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_.~'
    string.chars.map { |c| safe.include?(c) ? c : c.bytes.map { |b| "%#{b.to_s(16)}" } }.join
  end
end
